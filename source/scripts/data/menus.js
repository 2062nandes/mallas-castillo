export const menuinicio = [
  {
    title: 'Inicio',
    href: '',
    icon: 'icon-home'
  },
  {
    title: 'Nosotros',
    href: 'nosotros.html',
    icon: 'icon-us'
  },
  {
    title: 'Contactos',
    href: 'contactos.html',
    icon: 'icon-contact'
  }
]
export const mainmenu = [
  {
    title: 'Mallas olímpicas',
    href: 'mallas-olimpicas',
    list: [
      {
        title: '',
        href: ''
      },
      {
        title: 'Mallas olímpicas',
        href: '#',
        prod: [
          {
            title: 'Rollo de malla olímpica',
            src: 'mallas01-290px.jpg'
          },
          {
            title: `Malla olímpica <span class="strong">ROMBO</span> <span class="button--small button--ghost--cta strong t2">3x3</span>`,
            src: 'rombo3x3-290px.jpg'
          },
          {
            title: `Malla olímpica <span class="strong">ROMBO</span> <span class="button--small button--ghost--cta strong t2">4x4</span>`,
            src: 'rombo4x4-290px.jpg'
          },
          {
            title: `Malla olímpica <span class="strong">ROMBO</span> <span class="button--small button--ghost--cta strong t2">5x5</span>`,
            src: 'rombo5x5-290px.jpg'
          },
          {
            title: `Malla olímpica <span class="strong">ROMBO</span> <span class="button--small button--ghost--cta strong t2">6x6</span>`,
            src: 'rombo6x6-290px.jpg'
          },
          {
            title: `Malla olímpica <span class="strong">ROMBO</span> <span class="button--small button--ghost--cta strong t2">7x7</span>`,
            src: 'rombo7x7-290px.jpg'
          },
          {
            title: `Malla olímpica <span class="strong">ROMBO</span> <span class="button--small button--ghost--cta strong t2">8x8</span>`,
            src: 'rombo8x8-290px.jpg'
          },
          {
            title: 'Malla olímpica armada',
            src: 'mallas08-290px.jpg'
          }
        ]
      },
    ]
  },
  {
    title: 'Enmallados - Cerramientos',
    href: 'enmallados-cerramientos',
    list: [
      {
        title: '',
        href: ''
      },
      {
        title: 'Enmallados - Cerramientos',
        href: '#',
        prod: [
          {
            title: 'Enmallados al ras del suelo',
            src: 'enmallados01-290px.jpg'
          },
          {
            title: 'Mallas al ras del suelo',
            src: 'ras-del-suelo11-290px.jpg'
          },
          {
            title: 'Enmallados con muro de ladrillos',
            src: 'enmallados02-290px.jpg'
          },
          {
            title: 'Muro de ladrillo del enmallado',
            src: 'muro-de-ladrillo01-290px.jpg'
          },
          {
            title: 'Plantado de postes para Vallas',
            src: 'plantado-de-poste01-290px.jpg'
          },
          {
            title: 'Vallas protectoras',
            src: 'enmallados03-290px.jpg'
          },
          {
            title: 'Enmallados al ras del suelo',
            src: 'ras-del-suelo12-290px.jpg'
          },
          {
            title: 'Mallas al ras del suelo',
            src: 'ras-del-suelo13-290px.jpg'
          },
          {
            title: 'Enmallados anclados al suelo',
            src: 'enmallados04-290px.jpg'
          },
          {
            title: 'Mallas para cerco perimetral',
            src: 'vallas-olimpicas12-290px.jpg'
          },
          {
            title: 'Mallas ancladas al suelo',
            src: 'anclado-al-suelo11-290px.jpg'
          }
        ]
      },
    ]
  },
  {
    title: 'Alambres',
    href: 'alambres',
    list: [
      {
        title: '',
        href: ''
      },
      {
        title: 'Alambres',
        href: '#',
        prod: [
          {
            title: 'Alambre #16',
            src: 'alambre-16-290px.jpg'
          },
          {
            title: 'Alambre #14',
            src: 'alambre-14-290px.jpg'
          },
          {
            title: 'Alambre #12',
            src: 'alambre-12-290px.jpg'
          },
          {
            title: 'Alambre #10',
            src: 'alambre-10-290px.jpg'
          }
        ]
      },
    ]
  },
  {
    title: 'Postes de hormigón armado',
    href: 'postes-de-hormigon-armado',
    list: [
      {
        title: '',
        href: ''
      },
      {
        title: 'Postes de hormigón armado',
        href: '#',
        prod: [
          {
            title: 'Poste de hormigon armado',
            src: 'poste01-290px.jpg'
          },
          {
            title: 'Poste de hormigon de 3 mts.',
            src: 'poste02-290px.jpg'
          },
          {
            title: 'Poste de hormigon de 3.5 mts.',
            src: 'poste03-290px.jpg'
          },
          {
            title: 'Poste de hormigon para esquina',
            src: 'poste04-290px.jpg'
          }
        ]
      }
    ]
  }
]