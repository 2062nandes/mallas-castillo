import { mainmenu } from "../data/menus"
import { path_media, path_page } from "../data/routes"
export const galleryVue = {
  template: `
            <div class="rex-item l-100 rex-container gallery main-center">
              <template v-for="item in mainmenu[idmenu].list">
                <template v-for="subitem in item.prod">
                  <div class="rex-item l-25 m-40 s-90" v-if="item.title == title">
                    <figure class="gallery__item">
                    <div class=""><img class="" :src="path_media + 'images/' + mainmenu[idmenu].href + '/' + subitem.src" :alt="item.title"></div>
                    <figcaption class="" v-html="subitem.title"></figcaption>
                    </figure>
                  </div>
                </template> 
              </template>
            </div>
            `,
  data() {
    return {
      path_page,
      path_media,
      mainmenu
    }
  },
  props: {
    idmenu: {
      required: true
    },
    title: {
      required: true
    }
  }
}
